/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import nl.timogoudzwaard.medewerkervandemaandapp.R;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.ApplyText;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.Image;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.IntentCreator;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.UriFormatter;

import java.io.File;

import static nl.timogoudzwaard.medewerkervandemaandapp.MainActivity.LOAD_IMAGE_RESULT;

public class ShareFragment extends Fragment {
    private Button shareButton;
    private Button selectImageButton;
    private TextView textView;
    private ImageView selectedImage;
    private IntentCreator intentCreator;
    private ApplyText applyText;
    final private Image imageClass = Image.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_share, container, false);
        selectedImage = root.findViewById(R.id.imageviewShare);
        textView = root.findViewById(R.id.text_notifications);
        shareButton = root.findViewById(R.id.share_button);
        selectImageButton = root.findViewById(R.id.selectImageShare);
        intentCreator = new IntentCreator();
        applyText = new ApplyText();

        // initialize
        startAndResumeInitialize();

        // listen for select image button click
        setSelectImageShareClickListener();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        startAndResumeInitialize();
    }

    private void setShareButtonClickListener() {
        shareButton.setOnClickListener(view -> {
            shareImage();
        });
    }

    private void setSelectImageShareClickListener() {
        selectImageButton.setOnClickListener(view -> {
            getActivity().startActivityForResult(intentCreator.createSelectImageIntent(), LOAD_IMAGE_RESULT);
        });
    }

    private void shareImage() {
        final Image imageClass = Image.getInstance();
        final UriFormatter uriFormatter = new UriFormatter();
        Uri finalUri = imageClass.getImage();

        //  if not already formatted, get uri from intent and format to FileProvider approved URI for sharing files (it's not formatted when the user uses the SDK to select the image)
        if (!imageClass.getImage().toString().contains("content")) {
            finalUri = uriFormatter.formatUriToContentUri(imageClass.getImage().getPath(), getActivity());
        }

        // create intent for sharing the image
        String intentText = getResources().getText(R.string.send_to).toString();
        Intent shareIntent = intentCreator.createShareIntent(getActivity(), finalUri, intentText);
        startActivity(shareIntent);
    }

    private void startAndResumeInitialize() {
        if (imageClass.getImage() != null) {
            setShareButtonClickListener();
            applyText.setTextTextView(textView, getText(R.string.click_to_share).toString());
            selectedImage.setImageURI(imageClass.getImage());
            shareButton.setEnabled(true);
        } else {
            shareButton.setEnabled(false);
            applyText.setTextTextView(textView, getText(R.string.select_image_to_share).toString());
        }
    }
}