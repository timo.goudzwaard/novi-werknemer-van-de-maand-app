/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import ly.img.android.pesdk.backend.model.state.LoadSettings;
import ly.img.android.pesdk.backend.model.state.manager.SettingsList;
import ly.img.android.pesdk.ui.activity.EditorBuilder;
import nl.timogoudzwaard.medewerkervandemaandapp.R;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.*;
import org.w3c.dom.Text;

import static nl.timogoudzwaard.medewerkervandemaandapp.MainActivity.PESDK_RESULT;

public class EditFragment extends Fragment {
    private Button editButton;
    private Button employeeOfTheMonthButton;
    private TextView employeeOfTheMonthText;
    private ImageView selectedImage;
    private ApplyText applyText;
    final private Image imageClass = Image.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_edit, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);

        selectedImage = root.findViewById(R.id.imageViewEdit);
        editButton = root.findViewById(R.id.edit_image_button);
        employeeOfTheMonthButton = root.findViewById(R.id.employeeOfTheMonthButton);
        employeeOfTheMonthText = root.findViewById(R.id.employeeOfTheMonthText);
        applyText = new ApplyText();


        // set edit image text
        applyText.setTextTextView(textView, getText(R.string.edit_image_text).toString());

        // Invisible on create, only after the user presses the button the text will show up.
        employeeOfTheMonthText.setVisibility(View.INVISIBLE);

        // initialize
        startAndResumeInitialize();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        startAndResumeInitialize();
    }

    private void startAndResumeInitialize() {
        if(imageClass.getImage() != null) {
            setEditButtonClickListener();
            setEmployeeOfTheMonthButtonListener();
            selectedImage.setImageURI(imageClass.getImage());
        } else {
            editButton.setEnabled(false);
            employeeOfTheMonthButton.setEnabled(false);
        }
    }

    private void setEditButtonClickListener() {
        editButton.setOnClickListener(view -> {
            IntentCreator intentCreator = new IntentCreator();
            intentCreator.openEditor(getActivity());
        });
    }

    private void setEmployeeOfTheMonthButtonListener() {
        Month month = new Month();
        employeeOfTheMonthButton.setOnClickListener(view -> {
            applyText.setTextTextView(employeeOfTheMonthText, month.getEmployeeOfTheMonthText(getActivity()));
            employeeOfTheMonthText.setVisibility(View.VISIBLE);
        });
    }
}