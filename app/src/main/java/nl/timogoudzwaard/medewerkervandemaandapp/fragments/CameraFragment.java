/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import nl.timogoudzwaard.medewerkervandemaandapp.R;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.ApplyText;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.Image;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.IntentCreator;

import static nl.timogoudzwaard.medewerkervandemaandapp.MainActivity.LOAD_IMAGE_RESULT;

public class CameraFragment extends Fragment {
    private View root;
    private TextView textView;
    private ApplyText applyText;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_camera, container, false);
        textView = root.findViewById(R.id.text_home);
        applyText = new ApplyText();

        // set default text
        applyText.setTextTextView(textView, getText(R.string.button_click_to_take_picture).toString());

        // activate listener for camera button and image selector
        setCameraListener();
        setCameraImageSelectListener();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Image imageClass = Image.getInstance();

        if (imageClass.getImage() != null) {
            applyText.setTextTextView(textView, getText(R.string.picture_taken_text).toString());

            Button button = root.findViewById(R.id.camera_button);
            applyText.setButtonText(button, getText(R.string.select_different_picture).toString());
        } else {
            applyText.setTextTextView(textView, getText(R.string.button_click_to_take_picture).toString());
        }
    }

    private void setCameraListener() {
        final Button cameraButton = root.findViewById(R.id.camera_button);
        cameraButton.setOnClickListener(view -> {
            IntentCreator intentCreator = new IntentCreator();
            intentCreator.openCamera(getActivity());
        });
    }

    private void setCameraImageSelectListener() {
        final Button imageSelectButton = root.findViewById(R.id.camera_select_image);
        imageSelectButton.setOnClickListener(view -> {
            IntentCreator intentCreator = new IntentCreator();
            getActivity().startActivityForResult(intentCreator.createSelectImageIntent(), LOAD_IMAGE_RESULT);
        });
    }
}