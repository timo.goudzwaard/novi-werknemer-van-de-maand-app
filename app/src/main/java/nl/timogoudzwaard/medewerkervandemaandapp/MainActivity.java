/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import ly.img.android.pesdk.backend.model.EditorSDKResult;
import ly.img.android.pesdk.ui.utils.PermissionRequest;
import nl.timogoudzwaard.medewerkervandemaandapp.classes.Image;

public class MainActivity extends AppCompatActivity implements PermissionRequest.Response {
    public static final int PESDK_RESULT = 1;
    public static final int LOAD_IMAGE_RESULT = 2;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionRequest.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void permissionGranted() {}

    @Override
    public void permissionDenied() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_camera, R.id.navigation_edit, R.id.navigation_share)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, android.content.Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK && requestCode == PESDK_RESULT) {
            EditorSDKResult data = new EditorSDKResult(intent);
            data.notifyGallery(EditorSDKResult.UPDATE_RESULT & EditorSDKResult.UPDATE_SOURCE);

            // save result image in the Image class
            saveImage(data.getResultUri());

        } else if (requestCode == LOAD_IMAGE_RESULT && resultCode == RESULT_OK && intent != null) {
            saveImage(intent.getData());
        }
    }

    private void saveImage(Uri imageUri) {
        Image imageClass = Image.getInstance();
        imageClass.setImage(imageUri);
    }
}
