/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.classes;

import android.app.Activity;
import android.net.Uri;
import androidx.core.content.FileProvider;
import java.io.File;

public class UriFormatter {
    public Uri formatUriToContentUri(String imagePath, Activity activity) {
        File imageFile = new File(imagePath);
        Uri contentUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", imageFile);

        return contentUri;
    }
}
