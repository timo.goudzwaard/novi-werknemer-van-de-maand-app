/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.classes;

import android.widget.Button;
import android.widget.TextView;

public class ApplyText {
    public void setTextTextView(TextView textView, String string) {
        textView.setText(trimString(string));
    }

    public void setButtonText(Button button, String string) {
        button.setText(trimString(string));
    }

    private String trimString(String string) {
        String formattedString;
        formattedString = string.trim();

        return formattedString;
    }
}
