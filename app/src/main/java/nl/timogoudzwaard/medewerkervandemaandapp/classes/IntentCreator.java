/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.classes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import ly.img.android.pesdk.backend.model.state.LoadSettings;
import ly.img.android.pesdk.backend.model.state.manager.SettingsList;
import ly.img.android.pesdk.ui.activity.CameraPreviewBuilder;
import ly.img.android.pesdk.ui.activity.EditorBuilder;

import static nl.timogoudzwaard.medewerkervandemaandapp.MainActivity.PESDK_RESULT;

public class IntentCreator {
    public Intent createShareIntent(Context context, Uri imageUri, String intentText) {
        Month month = new Month();
        Intent shareIntent = new Intent();

        // configure the intent
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.putExtra(Intent.EXTRA_TEXT, month.getEmployeeOfTheMonthText(context));
        shareIntent.setType("image/jpeg");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        return Intent.createChooser(shareIntent, intentText);
    }

    public Intent createSelectImageIntent() {
        Intent pickImageIntent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        return pickImageIntent;
    }

    public void openCamera(Activity activity) {
        SettingsList settingsList = SDKSettings.getImageEditorSettings();

        new CameraPreviewBuilder(activity)
                .setSettingsList(settingsList)
                .startActivityForResult(activity, PESDK_RESULT);
    }

    public void openEditor(Activity activity) {
        SettingsList settingsList = SDKSettings.getImageEditorSettings();
        Image imageClass = Image.getInstance();

        // set input image
        settingsList.getSettingsModel(LoadSettings.class).setSource(imageClass.getImage());

        // open image editor
        new EditorBuilder(activity)
                .setSettingsList(settingsList)
                .startActivityForResult(activity, PESDK_RESULT);
    }
}
