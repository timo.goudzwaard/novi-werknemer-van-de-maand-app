/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.classes;

import android.content.Context;
import nl.timogoudzwaard.medewerkervandemaandapp.R;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class Month {
    public String getCurrentMonth() {
        String currentMonth;

        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();

        switch (month) {
            case 1:
                currentMonth = "januari";
                break;
            case 2:
                currentMonth = "februari";
                break;
            case 3:
                currentMonth = "maart";
                break;
            case 4:
                currentMonth = "april";
                break;
            case 5:
                currentMonth = "mei";
                break;
            case 6:
                currentMonth = "juni";
                break;
            case 7:
                currentMonth = "juli";
                break;
            case 8:
                currentMonth = "augustus";
                break;
            case 9:
                currentMonth = "september";
                break;
            case 10:
                currentMonth = "oktober";
                break;
            case 11:
                currentMonth = "november";
                break;
            case 12:
                currentMonth = "december";
                break;
            default:
                currentMonth = "Maand niet gevonden";
                break;
        }
        return currentMonth;
    }

    public String getEmployeeOfTheMonthText(Context context) {
        return context.getText(R.string.employee_otm) + " " + getCurrentMonth();
    }
}
