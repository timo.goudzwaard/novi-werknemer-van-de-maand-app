/*
 * Timo Goudzwaard
 * Software Development Praktijk 1
 * 07-07-2020
 */

package nl.timogoudzwaard.medewerkervandemaandapp.classes;

import android.net.Uri;

// This class is a Singleton
public class Image {
    private static Image INSTANCE = null;
    private static Uri imageUri = null;

    private Image() {};

    public static Image getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Image();
        }
        return(INSTANCE);
    }

    public Uri getImage() {
        return imageUri;
    }

    public void setImage(Uri image) {
        imageUri = image;
    }
}
